# Few-Shot Object Detection Challenge

This challenge aims to improve attention mechanisms for Few-Shot Object
Detection (FSOD). It is based on the AAF Framework, a modular attention
framework that allows easy implementation and comparison of existing methods
(see this [paper](https://arxiv.org/abs/2210.13923)). The main idea of the
challenge is to allow participants to modify exclusively the attention modules
of the framework, keeping all other parameters fixed, ensuring a fair comparison. 
## Getting started

### Install the framework 

Follow the instructions provided in  [`INSTALL.md`](/INSTALL.md) to install the required
dependancies and setup your environment. 

### Download dataset and configure paths

To download the dataset : 
```
mkdir datasets
cd datasets
wget data_url
tar -xzvf filename.tar.gz
```

Change the variables `DATA_DIR` and `DATASETS` in the file
`detlaf/config/path_catalog.py` to add the correct dataset paths according to
your setup. `DATA_DIR` should contain the path of a directory gathering all your
datasets. Then for each entry into the `DATASETS` dictionary, paths to the image
directory and annotation file should be provided. 

More explanation about dataset organization can be found [`/DATA.md`](/DATA.md).

In `detlaf/config/path_catalog.py`, four path variables must be set as well:
- `BASE_FOLDER`: path where models will be saved, each training creates a dedicated folder inside the base folder.
- `AAF_CONFIG_FOLDER`: path to the AAF config files, it should be something like `challenge_root/challenge/config_file/aaf_module/`
- `RESULTS_FOLDER`: path where the results of the evaluation will be stored.
- `TBOARD_LOGS_PATH`: path where the tensorboard logs will be saved.

## AAF Framework

The AAF Framework is a modular framework that can be used to easily implement
novel FSOD methods. It contains three main components: Spatial Alignement,
Global Attention and Feature Fusion. Each component can be tweaked independently
from the others. A thorough description of the framework is available
[here](/detlaf/README.md) and in this [paper](https://arxiv.org/abs/2210.13923).
The framework is at the heart of the challenge as participants are only allowed
to propose modifications of the three modules inside the framework. Details
about how to implement and test these modifications are given in the following
sections.


## Challenge participation

### Implementing new attention mechanisms

As mentioned before, participants are only allowed to modify the code from the
three modules of the framework. All hyperparameters that can be modified by the
participants are listed in the main config file. The baseline configuration is
available under `/challenge/config_files/fcos_R_50_FPN_DOTA.yaml`. The AAF
Framework however is controlled by a dedicated configuration file (examples are
available under `/challenge/config_files/aaf_modules`) and different AAF
Framework configurations can be selected from the main config file with the
field `FEWSHOT.AAF.CFG` which should be set to the path of the AAF config file
selected. 

New modules can be implemented by the participant to complement the ones already
available in the DETLAF package. To do so, participants should leverage the
python files inside `challenge/aaf_modules`. Each new module should be
implemented as a new class inheriting from the base modules classes (i.e.
`BaseAlignment`, `BaseAttention`, or `BaseFusionModule`). In addition, each new
module should be registered with a unique name using one of the three decorators
available in `detlaf.utils.registry` (e.g.
`@registry.ALIGNMENT_MODULE.register("NEW_MODULE_NAME")` for the alignment
module). The name register will then be used to specify what module should be
employed inside the AAF config file.

AAF configuration may also be extended by the participants to ease their
experimentations. This could be done by adding config nodes into
`challenge/aaf_modules/config/defaults.py`. Auxiliary files may also be used by
the participants to define useful classes or functions outside the three main
files (`alignement.py`, `attention.py`, and `fusion.py`). 

The organizers of the challenge will be available during the competition period
to help the participants to get started with the framework.

### Implementation constraints
To ensure a fair comparison between submissions, only the files containing the
implementation of the framework's three modules (i.e. under
`challenge/aaf_modules/` folder) may be modified by the participants, along with
the configuration files. No modification inside the `detlaf` package will be
allowed and verification will be done for each submission at the end of the
competition.

### Submission
Participants will be asked to submit the code of their proposed AAF modules
along with their configuration files. In addition, they should produce an
annotation file containing the predictions of their trained model on the test set. 

To ensure both reproducibility of the proposed method and generalization on
other sets of novel classes, new training will be done by the organizers.

### Evaluation
The organizers will evaluate the submissions on a reserved test set (for which annotations will not be made available to the participants) and on a few different class splits (also unknown to the participants). To assess the detection performance, mean Average Precision (mAP) will be used with an IoU threshold of 0.5 and averaged over base and novel classes separately. The results will be aggregated over 10 evaluation episodes, each with different support sets. The overall ranking of the participants is based on the performance on novel classes. However, submissions with sensibly degraded base classes performance will be disqualified.

## Training and evaluation tools

### Training 

Training is done in two phases: _base training_ and _fine-tuning_. During base
training, only base classes annotations are available while during fine-tuning,
a few annotations for novel classes are available. Specifically, 10 images with
their annotations are made available for each novel class. Training is handled
by the `Trainer` class and its method `train` which launches successively the
two training phases. Training is controlled by a set of parameters, most of them
are fixed and can be seen in the default config file
`detlaf/config/defaults.py`. Only a few parameters can be modified for this
challenge, they are listed and described in the main config file:
`challenge/config_files/fcos_R_50_FPN_DOTA.yaml`. 

Two python scripts are provided as examples to launch experiments
`challenge/single_training.py` and `challenge/batch_experiment.py`. The former
simply reads a config file and runs the training. The latter is more elaborated
and provides a simple way to run multiple experiments. It reads a config file
and launches a series of experimentations based on an `exp_list` variable.

`exp_list` should contain a list of dictionaries that have as keys the
parameters to be modified from the original config file and as values the list of parameters values. 
The length of the lists of values should be identical within each experiment set.

``` 
exp_list = [
    {'PARAM_1': [value_1, value_2], # first experiment set contains 2 experiments.
     'PARAM_2': [value_A, value_B]} , 
    {'PARAM_3': [value_X, value_Y, value_Z]} # second experiment set contains 3 experiments.
]
```
In the above example, 5 experiments will be launched by this script. It will
create a dedicated folder for each experiment and save the corresponding config
file in it. 

Finally, it will launch the evaluation for all models trained. 

### Evaluation
The `Eval` class allows to compute the performance separately on base and novel
classes. An evaluation script in `detlaf/eval/eval_script.py` allows the
evaluation of several models in a row.   


