from setuptools import setup


# To compile and install locally run "python setup.py build_ext --inplace"
# To install library to Python site-packages run "python setup.py build_ext install"

setup(
    name='detlaf',
    packages=['detlaf'],
    package_dir = {'detlaf': 'detlaf'},
    install_requires=['kornia>=0.6.6',
                        'timm>=0.5.4',
                        'pycocotools',
                        'pycocosiou>=1.5',
                        'tensorboard',
                        'rich',
                        'pandas'],
    version='1.0',
)
