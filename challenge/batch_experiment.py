import sys
import torch
import os

from detlaf.train.train import Trainer
from detlaf.eval.eval_script import eval_methods
from detlaf.config import cfg
from detlaf.train.experimenter import Experimenter

import aaf_modules

os.environ['OMP_NUM_THREADS'] = "4"
torch.backends.cudnn.benchmark = True


cfg_file = 'aaf_framework/config_files/fcos_R_50_FPN_DOTA_baseline.yaml'

exp_list = [
    {
        'MODEL.FCOS.IOU_LOSS_TYPE': ['gsiou', 'giou'],
    },

]

experimenter = Experimenter(cfg_file, initial_cfg_list, repeat=0, eval_ep=10)
experimenter.launch_from_list(exp_list)