from detlaf.aaf_framework.utils import AAFIOMode, BackgroundAttenuationBlock
from detlaf.utils import registry
from detlaf.aaf_framework.attention import BaseAttention


@registry.ATTENTION_MODULE.register("BGA_CHALLENGE")
class BackgroundAttentionChallenge(BaseAttention):
    def __init__(self, *args):
        super(BackgroundAttentionChallenge, self).__init__(*args)

        self.pooled_vectors = None

        self.ba_block = BackgroundAttenuationBlock(256,5, self.cfg)

        self.in_mode = AAFIOMode.Q_BCHW_S_NCHW
        self.out_mode = AAFIOMode.Q_BCHW_S_NCHW


    def forward(self, features):
        query_features = features['query' + self.input_name]
        support_features = features['support' + self.input_name]
        support_targets = features['support_targets']

        support_features = self.ba_block(support_features)

        features.update({
            'query' + self.output_name: query_features,
            'support' + self.output_name: support_features
        })