from detlaf.aaf_framework.utils import AAFIOMode
from detlaf.utils import registry
from detlaf.aaf_framework.fusion import BaseFusionModule

@registry.FUSION_MODULE.register("HADAMARD_CHALLENGE")
class FusionHadamardChallenge(BaseFusionModule):
    def __init__(self, *args):
        super(FusionHadamardChallenge, self).__init__(*args)

    def forward(self, features):

        query_features = features['query' + self.input_name]
        support_features = features['support' + self.input_name]
        support_targets = features['support_targets']

        query_support_merged = []
        for query, support in zip(query_features, support_features):
            Ns, B, C, Hs, Ws = support.shape
            B, Ns, C, Hq, Wq = query.shape

            assert Hs == Hq and Ws == Wq, 'Incompatible attention for this fusion module'

            feature_merged = query * support.permute(1, 0, 2, 3, 4)

            query_support_merged.append(feature_merged)

        features.update({self.output_name: query_support_merged})