import sys
import torch
import os

from detlaf.train.train import Trainer
from detlaf.eval.eval_script import eval_methods
from detlaf.config import cfg
from detlaf.train.experimenter import Experimenter

import aaf_modules

os.environ['OMP_NUM_THREADS'] = "4"
torch.backends.cudnn.benchmark = True

cfg.merge_from_file('challenge/config_files/fcos_R_50_FPN_DOTA.yaml')

trainer = Trainer(cfg)
trainer.train()