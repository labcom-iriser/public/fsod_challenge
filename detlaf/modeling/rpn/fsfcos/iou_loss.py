# GIoU and Linear IoU are added by following
# https://github.com/yqyao/FCOS_PLUS/blob/master/maskrcnn_benchmark/layers/iou_loss.py.
import torch
from torch import nn


class IOULoss(nn.Module):
    def __init__(self, cfg):
        super(IOULoss, self).__init__()
        self.cfg = cfg
        self.loss_type = cfg.MODEL.FCOS.IOU_LOSS_TYPE

    def forward(self, pred, target, weight=None):

        if self.loss_type == 'iou':
            ious = self.compute_iou(pred, target)
            losses = -torch.log(ious)
        elif self.loss_type == 'linear_iou':
            ious = self.compute_iou(pred, target)
            losses = 1 - ious
        elif self.loss_type == 'giou':
            gious = self.compute_giou(pred, target)
            losses = 1 - gious
        elif self.loss_type == 'siou':
            gamma = self.cfg.MODEL.FCOS.IOU_LOSS_PARAMS.SIOU_GAMMA
            img_size = self.cfg.MODEL.FCOS.IOU_LOSS_PARAMS.SIOU_SIZE
            losses = 1 - self.compute_siou(pred, target, gamma=gamma, img_size=img_size)
        elif self.loss_type == 'gsiou':
            gamma = self.cfg.MODEL.FCOS.IOU_LOSS_PARAMS.SIOU_GAMMA
            img_size = self.cfg.MODEL.FCOS.IOU_LOSS_PARAMS.SIOU_SIZE
            losses = 1 - self.compute_gsiou(
                pred, target, gamma=gamma, img_size=img_size)
        elif self.loss_type == 'giou_rectified':
            gamma = self.cfg.MODEL.FCOS.IOU_LOSS_PARAMS.SIOU_GAMMA
            img_size = self.cfg.MODEL.FCOS.IOU_LOSS_PARAMS.SIOU_SIZE
            losses = 1 - self.compute_giou_rectified(
                pred, target, img_size=img_size)
        elif self.loss_type == 'alpha_iou':
            alpha = self.cfg.MODEL.FCOS.IOU_LOSS_PARAMS.SIOU_GAMMA
            losses = 1 - self.compute_alphagiou(pred, target, alpha)
        elif self.loss_type == 'nwd':
            C = self.cfg.MODEL.FCOS.IOU_LOSS_PARAMS.NWD_C
            losses = 1 - self.compute_nwd(pred, target, C=C)
        else:
            raise NotImplementedError

        if weight is not None and weight.sum() > 0:
            return (losses * weight).sum()
        else:
            assert losses.numel() != 0
            return losses.sum()

    def compute_iou(self, pred, target):
        '''
        Careful, pred and target contains positive shifts from feature map
        position to each edge of the box (left, top, right, bottom).
        
        Center is computed as (right - left) / 2 and (top - bottom) / 2
        Height, width as ( top + bottom) and (right + left)
        '''

        pred_left = pred[:, 0]
        pred_top = pred[:, 1]
        pred_right = pred[:, 2]
        pred_bottom = pred[:, 3]

        target_left = target[:, 0]
        target_top = target[:, 1]
        target_right = target[:, 2]
        target_bottom = target[:, 3]

        target_area = (target_left + target_right) * \
                      (target_top + target_bottom)
        pred_area = (pred_left + pred_right) * \
                    (pred_top + pred_bottom)

        w_intersect = torch.min(pred_left, target_left) + torch.min(
            pred_right, target_right)
        h_intersect = torch.min(pred_bottom, target_bottom) + torch.min(
            pred_top, target_top)

        w_intersect = torch.where(w_intersect>= 0, w_intersect,
                                  torch.zeros_like(w_intersect))
        h_intersect = torch.where(h_intersect >= 0, h_intersect,
                                  torch.zeros_like(h_intersect))

        area_intersect = w_intersect * h_intersect
        area_union = target_area + pred_area - area_intersect
        ious = (area_intersect + 1.0) / (area_union + 1.0)

        # assess IoU is positive
        # gt sampling method (adaptive) can sample prediction without
        # intersection with gt
        ious = torch.where(ious >= 0, ious, torch.zeros_like(ious))
        return ious

    def compute_giou(self, pred, target):
        '''
        Careful, pred and target contains positive shifts from feature map
        position to each edge of the box (left, top, right, bottom).

        Center is computed as (right - left) / 2 and (top - bottom) / 2
        Height, width as ( top + bottom) and (right + left)
        '''
        pred_left = pred[:, 0]
        pred_top = pred[:, 1]
        pred_right = pred[:, 2]
        pred_bottom = pred[:, 3]

        target_left = target[:, 0]
        target_top = target[:, 1]
        target_right = target[:, 2]
        target_bottom = target[:, 3]

        target_area = (target_left + target_right) * \
                      (target_top + target_bottom)
        pred_area = (pred_left + pred_right) * \
                    (pred_top + pred_bottom)

        w_intersect = torch.min(pred_left, target_left) + torch.min(
            pred_right, target_right)
        g_w_intersect = torch.max(pred_left, target_left) + torch.max(
            pred_right, target_right)
        h_intersect = torch.min(pred_bottom, target_bottom) + torch.min(
            pred_top, target_top)
        g_h_intersect = torch.max(pred_bottom, target_bottom) + torch.max(
            pred_top, target_top)

        w_intersect = torch.where(w_intersect >= 0, w_intersect,
                                  torch.zeros_like(w_intersect))
        h_intersect = torch.where(h_intersect >= 0, h_intersect,
                                  torch.zeros_like(h_intersect))


        ac_uion = g_w_intersect * g_h_intersect + 1e-7
        area_intersect = w_intersect * h_intersect
        area_union = target_area + pred_area - area_intersect
        ious = (area_intersect + 1.0) / (area_union + 1.0)
        gious = ious - (ac_uion - area_union) / ac_uion
        if (gious < -1).sum().item() > 0:
            print(gious)
        return gious

    def compute_giou_rectified(self, pred, target, img_size=128):
        '''
        Careful, pred and target contains positive shifts from feature map
        position to each edge of the box (left, top, right, bottom).

        Center is computed as (right - left) / 2 and (top - bottom) / 2
        Height, width as ( top + bottom) and (right + left)
        '''
        pred_left = pred[:, 0]
        pred_top = pred[:, 1]
        pred_right = pred[:, 2]
        pred_bottom = pred[:, 3]

        target_left = target[:, 0]
        target_top = target[:, 1]
        target_right = target[:, 2]
        target_bottom = target[:, 3]

        target_area = (target_left + target_right) * \
                      (target_top + target_bottom)
        pred_area = (pred_left + pred_right) * \
                    (pred_top + pred_bottom)

        w_intersect = torch.min(pred_left, target_left) + torch.min(
            pred_right, target_right)
        g_w_intersect = torch.max(pred_left, target_left) + torch.max(
            pred_right, target_right)
        h_intersect = torch.min(pred_bottom, target_bottom) + torch.min(
            pred_top, target_top)
        g_h_intersect = torch.max(pred_bottom, target_bottom) + torch.max(
            pred_top, target_top)

        w_intersect = torch.where(w_intersect >= 0, w_intersect,
                                  torch.zeros_like(w_intersect))
        h_intersect = torch.where(h_intersect >= 0, h_intersect,
                                  torch.zeros_like(h_intersect))


        ac_uion = g_w_intersect * g_h_intersect + 1e-7
        area_intersect = w_intersect * h_intersect
        area_union = target_area + pred_area - area_intersect
        ious = (area_intersect + 1.0) / (area_union + 1.0)

        pred_area = torch.prod(pred[:, 2:] + pred[:, :2], dim=-1)
        target_area = torch.prod(target[:, 2:] + target[:, :2], dim=-1)

        mean_area = (pred_area + target_area) / 2
        alpha = 1 - torch.exp(-torch.sqrt(mean_area) / img_size)


        gious = ious - (1 - alpha) * (ac_uion - area_union) / ac_uion
        if (gious < -1).sum().item() > 0:
            print(gious)
        return gious


    def compute_siou(self, pred, target, gamma=0.5, img_size=128):
        ious = self.compute_iou(pred, target)
        # print(pred.shape)
        pred_area = torch.prod(pred[:, 2:] + pred[:, :2], dim=-1)
        target_area = torch.prod(target[:, 2:] + target[:, :2], dim=-1)

        mean_area = (pred_area + target_area) / 2
        # gamma = 1 + p * (torch.sqrt(mean_area) - img_size) / img_size
        p = 1 - (1 - gamma) * torch.exp(-torch.sqrt(mean_area) / img_size)

        return torch.pow(ious, p)

    def compute_gsiou(self, pred, target, gamma, img_size=128):
        gious = self.compute_giou(pred, target)

        pred_area = torch.prod(pred[:, 2:] + pred[:, :2], dim=-1)
        target_area = torch.prod(target[:, 2:] + target[:, :2], dim=-1)

        mean_area = (pred_area + target_area) / 2
        # gamma = 1 + p * (torch.sqrt(mean_area) - img_size) / img_size
        p = 1 - (1 - gamma) * torch.exp(-torch.sqrt(mean_area) / img_size)

        gsious = torch.where(gious > 0, torch.pow(gious.abs(), p),
                             -torch.pow(gious.abs(), p))
        return gsious

    def compute_alphagiou(self, pred, target, alpha):
        gious = self.compute_giou(pred, target)

        pred_area = torch.prod(pred[:, 2:] + pred[:, :2], dim=-1)
        target_area = torch.prod(target[:, 2:] + target[:, :2], dim=-1)


        gsious = torch.where(gious > 0, torch.pow(gious.abs(), alpha),
                            -torch.pow(gious.abs(), alpha))
        return gsious


    def compute_nwd(self, pred, target, C=12.7):
        '''
        Careful, pred and target contains positive shifts from feature map
        position to each edge of the box (left, top, right, bottom).
        
        Center is computed as (right - left) / 2 and (top - bottom) / 2
        Height, width as ( top + bottom) and (right + left)
        '''
        c_pred = (pred[:, 2:] - pred[:, :2]) / 2
        c_target = (target[:, 2:] - target[:, :2]) / 2

        wh_pred = pred[:, 2:] + pred[:, :2]
        wh_target = target[:, 2:] + target[:, :2]

        w_dist = torch.norm(c_pred - c_target, 2)  + torch.norm(
            (wh_pred - wh_target) / 2, 2)

        if C == 'adaptive':
            pred_area = torch.prod(wh_pred, dim=-1)
            target_area = torch.prod(wh_target, dim=-1)

            mean_area = (pred_area + target_area) / 2

            C = 2 * torch.log(mean_area)
        return torch.exp(-torch.sqrt(w_dist) / C)
