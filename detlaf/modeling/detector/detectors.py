# Copyright (c) Facebook, Inc. and its affiliates. All Rights Reserved.
from .generalized_rcnn import GeneralizedRCNN, FSGeneralizedRCNN


_DETECTION_META_ARCHITECTURES = {
    "GeneralizedRCNN": GeneralizedRCNN,
    "FSGeneralizedRCNN": FSGeneralizedRCNN
}


def build_detection_model(cfg):
    if cfg.FEWSHOT.NO_FS_PRETRAIN:
        meta_arch = GeneralizedRCNN
    else:
        meta_arch = _DETECTION_META_ARCHITECTURES[cfg.MODEL.META_ARCHITECTURE]
    return meta_arch(cfg)
