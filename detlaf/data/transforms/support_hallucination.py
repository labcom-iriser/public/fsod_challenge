import torch

from . import transforms as T
from .multiscale_cropper import MultiScaleSupport

class SupportHallucinator():
    def __init__(self, cfg, n_hallucination, modes=['augment'], keep_original=True, p=1.0):
        '''
        modes contains a list of augmentations 'augment', 'super_res', 'multiscale'
        '''
        self.cfg = cfg
        self.n_hallucination = n_hallucination
        self.keep_original = keep_original
        self.p = p
        self.modes = modes



        self.transforms = T.ComposeRandom([
                            # T.RandomVerticalFlip(0.5),
                            # T.RandomHorizontalFlip(0.5),
                            # T.ColorJitter(hue=0.1),
                            # T.RandomResizeCrop(128, p=0.5, level='image'),
                            # T.Cutout(p=.5, level='image'),
                            T.GaussianBlurBB(kernel_size=3)
                            ], p=self.p)

    def __call__(self, img, target):
        if self.keep_original:
            hallucinated_imgs, hallucinated_targets = [img], [target]
        else:
            hallucinated_imgs, hallucinated_targets = [], []

        for mode in self.modes:
            if mode == 'augment':
                for i in range(self.n_hallucination):
                    aug_img, aug_target = self.transforms(img, target)
                    hallucinated_imgs.append(aug_img)
                    hallucinated_targets.append(aug_target)



        hallucinated_imgs = torch.cat(hallucinated_imgs, dim=0)
        return hallucinated_imgs, hallucinated_targets
