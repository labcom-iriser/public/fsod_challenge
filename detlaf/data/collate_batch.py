# Copyright (c) Facebook, Inc. and its affiliates. All Rights Reserved.
from fcos_core.structures.image_list import to_image_list

from ..utils.visualization import display_support_images

class BatchCollator(object):
    """
    From a list of samples from the dataset,
    returns the batched images and targets.
    This should be passed to the DataLoader
    """

    def __init__(self, size_divisible=0):
        self.size_divisible = size_divisible

    def __call__(self, batch):
        transposed_batch = list(zip(*batch))
        images = to_image_list(transposed_batch[0], self.size_divisible)
        # display_support_images(images, 4)
        targets = transposed_batch[1]
        if len(targets) > 0 and isinstance(targets[0], list):
            targets = [t for target_list in targets for t in target_list]
        img_ids = transposed_batch[2]
        return images, targets, img_ids


class BBoxAugCollator(object):
    """
    From a list of samples from the dataset,
    returns the images and targets.
    Images should be converted to batched images in `im_detect_bbox_aug`
    """

    def __call__(self, batch):
        return list(zip(*batch))
