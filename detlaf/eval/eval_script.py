import os
import torch
import numpy as np
import logging

from shutil import copyfile

from ..config import cfg
from ..data.data_handler import DataHandler
from ..modeling.detector.detectors import build_detection_model
from ..utils.checkpointer import DetectronCheckpointer
from .eval import Evaluator

from ..utils.custom_logger import ProgressBar
from rich.logging import RichHandler

device = 'cuda:0'

FORMAT = "%(message)s"
logging.basicConfig(level="INFO",
                    format=FORMAT,
                    datefmt="[%X]",
                    handlers=[RichHandler()])
logger = logging.getLogger()


def eval_methods(model_names, base_folder, result_folder, n_episodes=10, shot_eval=[10], criterion='iou'):


    logger.info('Evaluation started with criterions: {}'.format(criterion))

    with ProgressBar() as pb:
        eval_task = pb.add_task("[red]", total=len(model_names))
        pb.episode_task = pb.add_task('[red]Episodes', total=n_episodes)
        pb.mini_episode_task = pb.add_task(
            '[red]Episode', total=10)

        for model_name in model_names:

            for k_shot in shot_eval:
                pb.update(
                    eval_task,
                    description="[red]Model {} {} shots".format(
                        model_name,
                        k_shot,
                        total=len(model_names) * len(shot_eval)))
                model_path = os.path.join(base_folder, model_name)
                model_file = "model_final_{}_shot.pth".format(k_shot)

                save_path = os.path.join(results_folder, model_name)

                with open(os.path.join(model_path, 'last_checkpoint'), 'w') as f:
                    f.write(model_file)

                cfg.merge_from_file(os.path.join(model_path, 'model_cfg.yaml'))
                cfg.merge_from_list(['FEWSHOT.K_SHOT', k_shot,
                                    'FEWSHOT.NO_FS_PRETRAIN', False])

                logger.info(cfg)

                model = build_detection_model(cfg)
                model.to(cfg.MODEL.DEVICE).eval()

                checkpointer = DetectronCheckpointer(cfg, model, save_dir=model_path, testing=True)
                _ = checkpointer.load()

                data_handler = DataHandler(cfg, is_train=False, start_iter=0, eval_all=True, data_source='test')
                # data_handler.is_train = True
                evaluator = Evaluator(model, cfg, data_handler, criterions=criterion, pb=pb)
                with torch.no_grad():
                    res = evaluator.eval_all(n_episode=n_episodes, seed=42, verbose=False)
                if not os.path.isdir(save_path):
                    os.makedirs(save_path)

                for crit, r in res.items():
                    if crit == 'iou':
                        res_path = os.path.join(save_path,
                                            model_file + '.csv')
                    else:
                        res_path = os.path.join(save_path,
                                                model_file + '_' + crit + '.csv')
                    r.to_csv(res_path, sep=',', index=None)

                pb.advance(eval_task)

                copyfile(os.path.join(model_path, model_file), os.path.join(save_path, model_file))
    logger.info('Evaluation done!')