import os, sys

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import matplotlib.colors
import numpy as np


def plot_single_img_boxes(image, boxes_list, cfg, backend=None, threshold=0.0, bgr=False, norm=False, ax=None):
    if backend is not None:
        matplotlib.use(backend)

    scale = 1
    if cfg.INPUT.PIXEL_MEAN[0] > 1:
        scale = 255
    if norm:
        img = (image.permute(1,2,0).cpu().numpy() * cfg.INPUT.PIXEL_STD + cfg.INPUT.PIXEL_MEAN) / scale
    else:
        img = image.permute(1,2,0).cpu().numpy()

    box_tensor = boxes_list.bbox
    labels = boxes_list.get_field('labels')
    colors = ['y' for i in range(len(labels))]
    if boxes_list.has_field('scores'):
        cmap = plt.get_cmap('viridis')
        colors = [cmap(s.cpu().detach().item()) for s in boxes_list.get_field('scores')]

    if ax is None:
        fig,ax = plt.subplots(1, figsize=(10,10))
    img = img.clip(0, 1)
    # Display the image
    if bgr:
        ax.imshow(img[:,:,::-1])
    else:
        ax.imshow(img)
    for idx_box, box in enumerate(box_tensor):
        x, y, w, h = box.cpu().detach().tolist()
        patch = patches.Rectangle((x,y),w-x,h-y,linewidth=2,edgecolor=colors[idx_box],facecolor='none')
        if not boxes_list.has_field('scores') or boxes_list.get_field('scores')[idx_box] >= threshold:
            ax.add_patch(patch)
            ax.text(x, y-5, labels[idx_box].item(), c='y')
    ax.axis('off')
    plt.savefig('test.png')
    plt.show()

def plot_img_boxes(images, boxes, index, cfg, backend=None, threshold=0.0):
    plot_single_img_boxes(images.tensors[index], boxes[index], cfg, backend=backend, threshold=threshold)

def plot_img_only(image, cfg=None):
    if cfg is not None:
        img = (image.permute(1, 2, 0).cpu().numpy() * cfg.INPUT.PIXEL_STD +
            cfg.INPUT.PIXEL_MEAN) / 255
    else:
        img = image - image.min()
        img /= img.max()
        img = img.permute(1, 2, 0).cpu().numpy()

    fig, ax = plt.subplots(1, figsize=(10, 10))
    img = img.clip(0, 1)
    ax.imshow(img[:, :, ::-1])
    plt.show()

def display_support_images(images_list, n_hallu):
    n_ways = len(images_list.tensors) // n_hallu # acutally n_ways * k_shots
    std = np.array([1.0, 1.0, 1.0]).reshape(3, 1, 1)
    mean = np.array([102.9801, 115.9465, 122.7717]).reshape(3, 1, 1)
    if len(images_list.tensors) % n_hallu != 0:
        return None

    fig, axs = plt.subplots(n_ways, n_hallu, figsize=(n_hallu * 5, n_ways * 5))
    for img_idx, img in enumerate(images_list.tensors):

        img = (img * std + mean) / 255
        img = img.permute(1, 2, 0)
        img = img.clip(0, 1)
        axs[img_idx // n_hallu, img_idx % n_hallu].imshow(img)
        axs[img_idx // n_hallu, img_idx % n_hallu].axis('off')
    plt.savefig('./support.png')


class HiddenPrints:
    def __init__(self, enabled=True):
        self.enabled = enabled

    def __enter__(self):
        if self.enabled:
            self._original_stdout = sys.stdout
            sys.stdout = open(os.devnull, 'w')

    def __exit__(self, exc_type, exc_val, exc_tb):
        if self.enabled:
            sys.stdout.close()
            sys.stdout = self._original_stdout