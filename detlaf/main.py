import sys
import torch
import os

from ..train.train import Trainer
from ..eval.eval_script import eval_methods
from ..config import cfg
from ..config.paths_catalog import BASE_FOLDER, RESULTS_FOLDER, AAF_CONFIG_FOLDER
from ..train.experimenter import Experimenter

os.environ['OMP_NUM_THREADS'] = "4"
torch.backends.cudnn.benchmark = True


# ###############################################################################

cfg.merge_from_file(
    'config_files/fcos_R_50_FPN_DOTA.yaml')
cfg.merge_from_list([
    'OUTPUT_DIR','/home/pierre/Documents/PHD/Experiments_FSFCOS/DOTA/TEST',
    'FEWSHOT.AAF.CFG', '/home/pierre/Documents/PHD/fsod_challenge/challenge/config_files/aaf_module/dana_challenge.yaml',
    'DATASETS.TRAIN', ("pascalv_merged_train", ),
    'DATASETS.TEST', ("pascalv_merged_test", ),
    'DATASETS.VAL', ("pascalv_merged_val", ),
    'MODEL.FCOS.NUM_CLASSES', 21,
    'FEWSHOT.ENABLED', True,
    'FEWSHOT.EPISODES', 1000,
    'FEWSHOT.K_SHOT', 1,
    'FINETUNE.EPISODES', 2000,
    'SOLVER.MAX_ITER', 100,
    'CONVIT_POS', False,
    'FEWSHOT.SUPPORT.CROP_MODE', 'RESIZE',
    'MODEL.FCOS.IOU_LOSS_TYPE', 'giou',
    'MODEL.FCOS.IOU_LOSS_PARAMS.SIOU_GAMMA', 5.0,
    'MODEL.FCOS.IOU_LOSS_PARAMS.SIOU_SIZE', 16,
    'MODEL.FCOS.IOU_LOSS_PARAMS.NWD_C', 12.7,
    # 'LOGGING.INTERVAL', 1,
    # 'LOSSES.ADAPTIVE_RADIUS_SAMPLE_REGION', True,
    # 'FINETUNE.NEGATIVE_SUP', False,
    'AUGMENT.VFLIP_PROBA', 0.0,
    'FEWSHOT.SUPPORT.AUGMENT.ENABLED', False,
    'FEWSHOT.SUPPORT.AUGMENT.HALLUCINATION', 0,
])



trainer = Trainer(cfg)
trainer.train()

method_to_eval = [
    'COCO_QSGA_IOU_LOSS_TYPE_GSIOU_2',
    'COCO_QSGA_IOU_LOSS_TYPE_GIOU_1'
]
eval_methods(method_to_eval,
             BASE_FOLDER,
             RESULTS_FOLDER,
             n_episodes=10,
             criterion=['iou', 'siou', 'nwd'])
