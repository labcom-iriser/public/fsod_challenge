# DETLAF: Detection Library for Attention methods in Few-shot regime. 

This repository contains a set of tools for designing, training and evaluating
Few-Shot Object Detection (FSOD) methods. Among other things, it provides a
modular framework called Attention Alignment Fuison Framework to ease the
re-implementation of existing methods and fair comparison. 


## AAF Framework generalities
This repository contains the code of the AAF framework proposed in this
[paper](https://arxiv.org/abs/2210.13923). The main idea behind this work is to propose
a flexible framework to implement various attention mechanisms for Few-Shot
Object Detection. The framework is composed of 3 different modules: Spatial Alignment, 
Global Attention and Fusion Layer, which are applied successively to combine 
features from query and support images. 

The inputs of the framework are:
- query_features `List[Tensor(B, C, Hq, Wq)]`: Query features at different levels. For each level, the features are of shape Batch x Channels x Height x Width.
- support_features `List[Tensor(N, C, Hs', Ws')]` : Support features at different level. The first dimension corresponds to the number of support images, regrouped by class: `N = N_WAY * K_SHOT`.
- support_targets `List[BoxList]` bounding boxes for objects in each support image. 

The three modules are called sequentially (first alignment then attention and finally fusion). Each module takes as input a feature dictionary, it selects its input features from the dictionary and writes its output features inside the same dict. This allows keeping track of intermediate features so that they can be combined in various ways in the fusion module in the end.

The keys of selected features by the first module are `"query_features"` and `"support_features"`. It then stores the processed features in the dictionary with key `"query_p1"` and `"support_p1"`. The second module takes from `"query_p1"` and `"support_p1"` and writes into  `"query_p2"` and `"support_p2"`. The final module (fusion) takes from `"query_p2"` and `"support_p2"` keys and writes into `"query_output_features"` and `"support_output_features"`. As Alignment and Attention application order can be swapped (using `ALIGN_FIRST` parameter), a check must be done when the framework is initialized to specify where each module should look and write in the feature dictionary. This is handled in the Base classes of the three modules (`BaseAlignment`, `BaseAttention`, and `BaseFusionModule`). 



The framework can be configured using a separate config file. Examples of such files are available under `/challenge/config_files/aaf_framework/`. The structure of these files is simple: 
```python
ALIGN_FIRST: #True/False Run Alignment before Attention when True
ALIGNMENT:
    MODE: # Name of the alignment module selected
ATTENTION:
    MODE: # Name of the attention module selected
FUSION:
    MODE: # Name of the fusion module selected
```
| File name                | Method                           | Alignment          | Attention         | Fusion      |
|--------------------------|----------------------------------|--------------------|-------------------|-------------|
| `identity.yaml`            | Identity                         | IDENTITY           | IDENTITY          | IDENTITY    |
| `feature_reweighting.yaml` | [FSOD via feature reweighting](https://arxiv.org/pdf/1812.01866v2.pdf)     | IDENTITY           | REWEIGHTING_BATCH | IDENTITY    |
| `meta_faster_rcnn.yaml`    | [Meta Faster-RCNN](https://arxiv.org/pdf/2104.07719.pdf)                 | SIMILARITY_ALIGN   | META_FASTER       | META_FASTER |
| `self_adapt.yaml`          | [Self-adaptive attention for FSOD](https://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=9426416) | IDENTITY_NO_REPEAT | GRU               | IDENTITY    |
| `dynamic.yaml`             | [Dynamic relevance learning](https://arxiv.org/pdf/2108.02235.pdf)       | IDENTITY           | INTERPOLATE       | DYNAMIC_R   |
| `dana.yaml`                | [Dual Awarness Attention for FSOD](https://arxiv.org/pdf/2102.12152v3.pdf) | CISA               | BGA               | HADAMARD    |

The path to the AAF config file should be specified inside the master config file (i.e. for the whole network) under `FEWSHOT.AAF.CFG`. 

For each module, classes implementing the available choices are regrouped under a single file: `/modelling/aaf/alignment.py`, `/modelling/aaf/attention.py` and `/modelling/aaf/fusion.py`.
### Spatial Alignment
Spatial Alignment reorganizes spatially the features of one feature map to match another one. The idea is to align similar features in both maps so that comparison is easier.

| Name               | Description                                                                                  |
|--------------------|----------------------------------------------------------------------------------------------|
| IDENTITY           | Repeats the feature to match BNCHW and NBCHW dimensions                                      |
| IDENTITY_NO_REPEAT | Identity without repetition                                                                  |
| SIMILARITY_ALIGN   | Compute similarity matrix between support  and query and align support to query accordingly. |
| CISA               | CISA block from [this method](https://arxiv.org/pdf/2102.12152.pdf)                          |

### Global Attention
Global Attention highlights some features of a map accordingly to an attention vector computed globally on another one. The idea is to leverage global and hopefully semantic information. 

| Name              | Description                                                                                            |
|-------------------|--------------------------------------------------------------------------------------------------------|
| IDENTITY          | Simply pass features to next modules.                                                                  |
| REWEIGHTING       | Reweights query features using globally pooled vectors from support.                                   |
| REWEIGHTING_BATCH | Same as above but support examples are the same  for the whole batch.                                  |
| SELF_ATTENTION    | Same as above but attention vectors are computed  from the alignment matrix between query and support. |
| BGA               | BGA blocks from [this method](https://arxiv.org/pdf/2102.12152.pdf)                                   |
| META_FASTER       | Attention block from [this method](https://arxiv.org/abs/2104.07719)                              |
| POOLING           | Pools query and support features to the same size.                                                     |
| INTERPOLATE       | Upsamples support features to match query size.                                                        |
| GRU               | Computes attention vectors through a graph  representation using a GRU.                                |
### Fusion Layer
Combine directly the features from support and query. These maps must be of the same dimension for point-wise operation. Hence fusion is often employed along with alignment. 

| Name        | Description                                                        |
|-------------|--------------------------------------------------------------------|
| IDENTITY    | Returns onlu adapted query features.                               |
| ADD         | Point-wise sum between query and support features.                 |
| HADAMARD    | Point-wise multiplication between query and support features.      |
| SUBSTRACT   | Point-wise substraction between query and support features.        |
| CONCAT      | Channel concatenation of query and support features.               |
| META_FASTER | Fusion layer from [this method](https://arxiv.org/abs/2104.07719)  |
| DYNAMIC_R   | Fusion layer from  [this method](https://arxiv.org/abs/2108.02235) |



## Datahandler

Explain datahandling mechanics 

## Base training vs Fine-tuning

What changes between the two phases of training

## Experimenter

How to use it ? 

## Evaluator

What does it evaluate ?
