import os
import logging
from rich.logging import RichHandler
import pandas as pd
import numpy as np
import traceback

from .train import Trainer
from ..eval.eval_script import eval_methods
from ..config import cfg
from ..config.paths_catalog import BASE_FOLDER, RESULTS_FOLDER, AAF_CONFIG_FOLDER

FORMAT = "%(message)s"
logging.basicConfig(level="INFO",
                    format=FORMAT,
                    datefmt="[%X]",
                    handlers=[RichHandler()])

class Experimenter():

    def __init__(self, cfg_file, 
                        initial_cfg, 
                        repeat=0, 
                        required_params=['MODEL.FCOS.IOU_LOSS_TYPE'],
                        discarded_params=['MODEL.WEIGHT'],
                        eval_ep=10):
        self.cfg_file = cfg_file
        self.initial_cfg = initial_cfg
        self.repeat = repeat
        self.eval_ep = eval_ep

        self.logger = logging.getLogger()

        self.output_dir = BASE_FOLDER
        self.aaf_config_path = AAF_CONFIG_FOLDER
        self.res_path = RESULTS_FOLDER

        self.required_params = required_params # Dataset and attention methods are treated separately
        self.discarded_params = discarded_params 

    def launch_from_list(self, exp_list):
        '''
        - list_exp: a list of dict with list of same length as values
                    if dict length is 2 and list length are different
                    a grid search is performed. 
                    Example: [
                                {p1: [v1, v2]}, 
                                {p2:[v3], p3:[v4]},  
                                {p2:[v3, v5], p3:[v4]}
                              ]
        '''
        cfg_dict = self.prepare_cfg_dict(exp_list)

        self.run_experiments(cfg_dict)

    def prepare_cfg_dict(self, exp_list):
        cfg_dict = {}
        for exp in exp_list:

            params_length = [len(values) for param, values in exp.items()]
            different_lengths = list(set(params_length))

            for idx, (param, values) in enumerate(exp.items()):
                if len(values) == 1 and len(different_lengths) > 1:
                    exp[param] = values * different_lengths[1]
                    params_length[idx] = different_lengths[1]

            if params_length.count(params_length[0]) == len(params_length):

                for i in range(params_length[0]):
                    current_cfg = []
                    name = ''
                    missing_dataset = True
                    missing_aaf = True
                    missing_params = {p: True for p in self.required_params}

                    for param, values in exp.items():

                        if 'DATASETS' in param:
                            name += values[i].upper() + '_'
                            missing_dataset = False
                            for split in ['train', 'val', 'test']:
                                current_cfg = current_cfg + ['DATASETS.{}'.format(values[i].upper()) ,
                                                ('{}_{}'.format(values[i], split),)]
                        elif param == 'FEWSHOT.AAF.CFG':
                            name += values[i].upper() + '_'
                            missing_aaf = False
                            current_cfg = current_cfg + [param, os.path.join(self.aaf_config_path, values[i] + '.yaml')]
                        else:
                            if param in missing_params:
                                missing_params[param] = False
                            if not param in self.discarded_params:
                                name += '{}_{}'.format(param.split('.')[-1].upper(), str(values[i]).upper()) + '_'
                                current_cfg = current_cfg + [param, values[i]]

                    if missing_aaf:
                        name = self.get_param_from_list(
                            self.initial_cfg, 'FEWSHOT.AAF.CFG').split('/')[-1].split('.')[0].upper() + '_' + name

                    if missing_dataset:
                        name = self.get_param_from_list(
                            self.initial_cfg, 'DATASETS.TRAIN')[0].split('_')[0].upper() + '_' + name

                    for param, missing in missing_params.items():
                        if missing:
                            name += str(self.get_param_from_list(self.initial_cfg, param)).upper() + '_'

                    name = name[:-1]
                    name = self.check_name(name)
                    current_cfg += ['OUTPUT_DIR', os.path.join(self.output_dir, name)]
                    cfg_dict[name] = current_cfg

            elif len(params_length) == 2:
                raise NotImplementedError
            else:
                self.logger.error('Grid search is only implemented in 2d, experiment skipped.')

            if len(cfg_dict) == 1 and self.repeat > 0:
                cfg_dict_repeated = {}
                name_exp = list(cfg_dict.keys())[0]
                exp_params = cfg_dict[name_exp]
                for exp_idx in range(self.repeat-1):
                    exp_params_i = exp_params.copy()
                    output_dir_id = exp_params_i.index('OUTPUT_DIR')
                    exp_params_i[output_dir_id +
                               1] = exp_params_i[output_dir_id +
                                               1] + '_{}'.format(exp_idx + 1)
                    cfg_dict_repeated[name + '_{}'.format(exp_idx + 1)] = exp_params_i.copy()


                cfg_dict.update(cfg_dict_repeated)

        return cfg_dict

    def prepare_grid_search(self, exp_dict):
        return {}

    def get_param_from_list(self, param_list, param):
        for idx, p in enumerate(param_list[::2]):
            if param == p:
                return param_list[2*idx + 1 ]

    def prettify_param_list(self, param_list):
        param_dict = dict(zip(param_list[::2], param_list[1::2]))
        param_str = ''
        for p, v in param_dict.items():
            param_str += '- {}: {}\n'.format(p.replace('_', '-'),
                                           str(v).replace('_', '-'))
        return param_str

    def run_experiments(self, params_dict):
        for model_name, params in params_dict.items():
            '''First reset cfg from default and intial configuration 
            (i.e. what changes from default but kept fixed during experiments)
            '''
            cfg.merge_from_file(self.cfg_file)
            cfg.merge_from_list(self.initial_cfg)

            # Then change novel params
            cfg.merge_from_list(params)

            try:
                trainer = Trainer(cfg)
                trainer.train()
            except Exception as err:
                self.logger.error(err)
                traceback.print_exc()
                continue

            try:
                eval_methods(['{}'.format(model_name)], 
                            BASE_FOLDER, 
                            RESULTS_FOLDER,
                            n_episodes=self.eval_ep, 
                            criterion=['iou', 'siou', 'nwd'])
            except Exception as err:
                self.logger.error(err)
                traceback.print_exc()
                continue

            try:
                res_location = os.path.join(RESULTS_FOLDER,
                                model_name)
                res = self.load_res(res_location)
                train_map = res['Train classes']['mean'][10, 1] * 100
                test_map = res['Test classes']['mean'][10, 1] * 100
            except Exception as err:
                self.logger.error(err)
                traceback.print_exc()


    def load_res(self, path, is_baseline=False, row=1, criterion='', verbose=False, k=10):
        results_path = path
        results = {}
        for file in os.listdir(results_path):

            if criterion == 'iou' or criterion == '':
                crit = ''
            elif criterion != '':
                crit = '_' + criterion

            end_of_file = 'pth' + crit + '.csv'
            if '{}_shot'.format(k) in file and file.endswith(end_of_file):
                if verbose:
                    print('Load results from {}'.format(file))
                name = file.split('_')[2]
                results[int(name)] = pd.read_csv(os.path.join(results_path, file), header=[0,1])
                break
            elif is_baseline and 'model_final.pth.csv' == file:
                if verbose:
                    print('Load results from baseline model.')
                results[1] = pd.read_csv(os.path.join(results_path, file), header=[0,1])
                break

        if results != {}:
            map50 = pd.concat(results).loc[pd.IndexSlice[:, row], :].sort_index()
            map50[map50 < 0.0] = np.nan
            map50['Train classes', 'mean'] = map50['Train classes'].mean(axis=1)
            if not is_baseline:
                map50['Test classes', 'mean'] = map50['Test classes'].mean(axis=1)
            return map50
        else:
            return None

    def check_name(self, name):
        existing_exp = os.listdir(self.output_dir)
        similar_exp = []
        shift = 0
        for exp in existing_exp:
            if name in exp:
                try:
                    exp_nb = int(exp.split('_')[-1])
                except:
                    exp_nb = 0
                if name == exp or (exp_nb != 0 and (name + '_' + str(exp_nb)) == exp):
                    shift = max(shift, exp_nb) + 1

        if shift > 0:
            name += '_' + str(shift)

        return name
