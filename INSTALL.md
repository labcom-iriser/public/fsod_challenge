# Setting the environment for the challenge

First, we recommend that you create a dedicated folder `challenge_root` in which
everything related to the challenge will be stored. 

## Virtual environment creation
First, create a conda environment for the challenge and install pytorch. 

```
conda create -n fsod_challenge
conda activate fsod_challenge
conda install python==3.8 setuptools==59.5.0 -c conda-forge
conda install pytorch==1.9.0 torchvision==0.10.0 cudatoolkit=11.1 -c pytorch -c conda-forge
```

Depending on your hardware, you may require older version of cudatoolkit. In
such a case, make sure to install the compatible versions of torch/torchvision,
see
[here](https://pytorch.org/get-started/previous-versions/#v182-with-lts-support).
It was also tested with pytorch==1.8.2, torchvision==0.9.2 and
cudatoolkit==10.2. 

## FCOS installation
The AAF FSOD package is based on FCOS and therefore requires it as a dependancy.
As FCOS is known to have a few installation issue that were not fixed in the
[original repository](https://github.com/tianzhi0549/FCOS), we forked it and
apply the required modification to ease the installation.

```
git clone https://github.com/pierlj/FCOS.git
cd FCOS
# Use GCC/G++ (>= 4.9,< 6.0)
python setup.py build install
cd ..
```

Please make sure to use compatible GCC and G++ versions (>= 4.9,< 6.0) as
described in the `INSTALL.md` file from FCOS repository.
[Here](https://askubuntu.com/questions/26498/how-to-choose-the-default-gcc-and-g-version)
is a good way to deal with several GCC/G++ versions without messing up your
system. This constraint forces the use of `cudatoolkit < 11.3`, see
[here](https://gist.github.com/ax3l/9489132).

# Installing FSOD AAF 
Lastly, install the `detlaf` package from this repository.

```
git clone git@gitlab.sorbonne-paris-nord.fr:labcom-iriser/public/fsod_challenge.git
cd fsod_challenge
pip install .
cd ..
```



